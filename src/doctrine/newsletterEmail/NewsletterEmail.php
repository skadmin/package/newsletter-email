<?php

declare(strict_types=1);

namespace Skadmin\NewsletterEmail\Doctrine\NewsletterEmail;

use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class NewsletterEmail
{
    use Entity\Id;
    use Entity\Created;
    use Entity\Email;
    use Entity\Hash;
    use Entity\IsActive;

    public function create(string $email, string $hash): void
    {
        $this->setEmail($email);
        $this->setHash($hash);

        $this->setIsActive(false);
    }
}
