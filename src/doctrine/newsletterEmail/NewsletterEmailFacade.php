<?php

declare(strict_types=1);

namespace Skadmin\NewsletterEmail\Doctrine\NewsletterEmail;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

use function assert;

final class NewsletterEmailFacade extends Facade
{
    use Facade\Hash;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = NewsletterEmail::class;
    }

    public function create(string $email): ?NewsletterEmail
    {
        if ($this->findByEmail($email) !== null) {
            return null;
        }

        $newsletterEmail = $this->get();
        $newsletterEmail->create($email, $this->getValidHash());

        $this->em->persist($newsletterEmail);
        $this->em->flush();

        return $newsletterEmail;
    }

    public function findByEmail(string $email): ?NewsletterEmail
    {
        $criteria = ['email' => $email];

        $newsletterEmail = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
        assert($newsletterEmail instanceof NewsletterEmail || $newsletterEmail === null);

        return $newsletterEmail;
    }

    public function get(?int $id = null): NewsletterEmail
    {
        if ($id === null) {
            return new NewsletterEmail();
        }

        $newsletterEmail = parent::get($id);

        if ($newsletterEmail === null) {
            return new NewsletterEmail();
        }

        return $newsletterEmail;
    }

    public function removeById(int $id): bool
    {
        return $this->remove($this->get($id));
    }

    public function remove(NewsletterEmail $newsletterEmail): bool
    {
        if ($newsletterEmail->isLoaded()) {
            $this->em->remove($newsletterEmail);
            $this->em->flush();

            return true;
        }

        return false;
    }

    public function activeByHash(string $hash): bool
    {
        $newsletterEmail = $this->findByHash($hash);

        if ($newsletterEmail === null || $newsletterEmail->isActive()) {
            return false;
        }

        $newsletterEmail->setIsActive(true);
        $this->em->persist($newsletterEmail);
        $this->em->flush();

        return true;
    }

    public function findByHash(string $hash): ?NewsletterEmail
    {
        $criteria = ['hash' => $hash];

        $newsletterEmail = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
        assert($newsletterEmail instanceof NewsletterEmail || $newsletterEmail === null);

        return $newsletterEmail;
    }
}
