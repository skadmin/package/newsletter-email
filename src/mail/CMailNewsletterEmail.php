<?php

declare(strict_types=1);

namespace Skadmin\NewsletterEmail\Mail;

use Skadmin\Mailing\Model\CMail;
use Skadmin\Mailing\Model\MailParameterValue;
use Skadmin\Mailing\Model\MailTemplateParameter;

use function sprintf;

class CMailNewsletterEmail extends CMail
{
    public const TYPE = 'newsletter-email';

    private string $email;
    private string $href;
    private string $link;

    public function __construct(string $email, string $href, string $link)
    {
        $this->email = $email;
        $this->href  = $href;
        $this->link  = $link;
    }

    /**
     * @return mixed[]
     */
    public static function getModelForSerialize(): array
    {
        $params = ['email', 'href', 'link'];
        $model  = [];

        foreach ($params as $param) {
            $description = sprintf('mail.newsletter-email.parameter.%s.description', $param);
            $example     = sprintf('mail.newsletter-email.parameter.%s.example', $param);

            $model[] = (new MailTemplateParameter($param, $description, $example))->getDataForSerialize();
        }

        return $model;
    }

    /**
     * @return MailParameterValue[]
     */
    public function getParameterValues(): array
    {
        return [
            new MailParameterValue('email', $this->getEmail()),
            new MailParameterValue('href', $this->getHref()),
            new MailParameterValue('link', $this->getLink()),
        ];
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): CMailNewsletterEmail
    {
        $this->email = $email;

        return $this;
    }

    public function getHref(): string
    {
        return $this->href;
    }

    public function setHref(string $href): CMailNewsletterEmail
    {
        $this->href = $href;

        return $this;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink(string $link): CMailNewsletterEmail
    {
        $this->link = $link;

        return $this;
    }
}
