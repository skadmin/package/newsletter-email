<?php

declare(strict_types=1);

namespace Skadmin\NewsletterEmail;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'newsletter-email';
    public const DIR_IMAGE = 'newsletter-email';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-mail-bulk']),
            'items'   => ['overview'],
        ]);
    }
}
