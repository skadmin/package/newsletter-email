<?php

declare(strict_types=1);

namespace Skadmin\NewsletterEmail\Components\Admin;

use App\Model\Grid\Traits;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use App\Model\System\Utils;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\NewsletterEmail\BaseControl;
use Skadmin\NewsletterEmail\Doctrine\NewsletterEmail\NewsletterEmail;
use Skadmin\NewsletterEmail\Doctrine\NewsletterEmail\NewsletterEmailFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Ublaboo\DataGrid\Column\Action\Confirmation\CallbackConfirmation;
use Ublaboo\DataGrid\Column\ColumnDateTime;
use Ublaboo\DataGrid\Column\ColumnText;

use function date;
use function intval;
use function sprintf;

class Overview extends GridControl
{
    use APackageControl;
    use Traits\IsActive;

    private NewsletterEmailFacade $facade;

    public function __construct(NewsletterEmailFacade $facade, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'newsletter-email.overview.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // COLUMNS
        $grid->addColumnText('email', 'grid.newsletter-email.overview.email')
            ->setRenderer(static function (NewsletterEmail $newsletterEmail): Html {
                $hash = Html::el('code', ['class' => 'text-muted small'])->setText($newsletterEmail->getHash());

                $render = new Html();
                $render->addHtml(Utils::createHtmlContact($newsletterEmail->getEmail()))
                    ->addHtml('<br/>')
                    ->addHtml($hash);

                return $render;
            });
        $grid->addColumnDateTime('createdAt', 'grid.newsletter-email.overview.created-at')
            ->setFormat('d.m.Y H:i');
        $this->addColumnIsActive($grid, 'newsletter-email.overview');

        // FILTER
        $grid->addFilterText('email', 'grid.newsletter-email.overview.email', ['email', 'hash']);
        $this->addFilterIsActive($grid, 'newsletter-email.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::DELETE)) {
            $confirmation   = new CallbackConfirmation(function (NewsletterEmail $newsletterEmail): string {
                $translation = new SimpleTranslation('grid.newsletter-email.overview.remove.confirmation - %s', $newsletterEmail->getEmail());

                return $this->translator->translate($translation);
            });
            $grid->addActionCallback('remove', 'grid.newsletter-email.overview.remove')
                ->setConfirmation($confirmation)
                ->setIcon('trash-alt')
                ->setTitle('grid.newsletter-email.overview.remove.title')
                ->setClass('btn btn-xs btn-outline-danger ajax')
                ->onClick[] = function ($id) use ($grid): void {
                    $newsletterEmail = $this->facade->get(intval($id));
                    $presenter       = $this->getPresenterIfExists();

                    if ($this->facade->remove($newsletterEmail)) {
                        $flashMessage = new SimpleTranslation('grid.newsletter-email.overview.action.flash.remove.success "%s"', $newsletterEmail->getEmail());
                        $type         = Flash::SUCCESS;
                    } else {
                        $flashMessage = new SimpleTranslation('grid.newsletter-email.overview.action.flash.remove.danger "%s"', $newsletterEmail->getEmail());
                        $type         = Flash::DANGER;
                    }

                    if ($presenter !== null) {
                        $presenter->flashMessage($flashMessage, $type);
                    }

                    $grid->reload();
                };
        }

        // TOOLBAR

        // OTHER
        $grid->setDefaultSort(['createdAt' => 'DESC']);
        $grid->setDefaultFilter([
            'isActive' => Constant::YES,
        ]);

        // EXPORT
        $this->addExport($grid);

        return $grid;
    }

    private function addExport(GridDoctrine &$grid): void
    {
        $columnEmail     = new ColumnText($grid, 'email', 'email', 'grid.newsletter-email.overview.export.email');
        $columnHash      = new ColumnText($grid, 'hash', 'hash', 'grid.newsletter-email.overview.export.hash');
        $columnIsActive  = (new ColumnText($grid, 'isActive', 'isActive', 'grid.newsletter-email.overview.export.is-active'))
            ->setReplacement([
                Constant::NO  => Constant::NO,
                Constant::YES => Constant::YES,
            ]);
        $columnCreatedAt = (new ColumnDateTime($grid, 'createdAt', 'createdAt', 'grid.newsletter-email.overview.export.created-at'))
            ->setFormat('Y-m-d H:i:s');

        $exportName = sprintf('newsletter-%s.csv', date('YmdHis'));
        $grid->addExportCsvFiltered('grid.newsletter-email.overview.csv-export', $exportName)
            ->setTitle('grid.newsletter-email.overview.csv-export.title')
            ->setClass('btn btn-xs btn-outline-primary')
            ->setIcon('download')
            ->setColumns([
                $columnEmail,
                $columnHash,
                $columnIsActive,
                $columnCreatedAt,
            ]);
    }
}
