<?php

declare(strict_types=1);

namespace Skadmin\NewsletterEmail\Components\Front;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\Utils\ArrayHash;
use Skadmin\NewsletterEmail\Doctrine\NewsletterEmail\NewsletterEmailFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormControl;
use WebLoader\Nette\LoaderFactory;

class FormSubscribe extends FormControl
{
    use APackageControl;

    private LoaderFactory         $webLoader;
    private NewsletterEmailFacade $facade;

    public function __construct(NewsletterEmailFacade $facade, Translator $translator, LoaderFactory $webLoader)
    {
        parent::__construct($translator);
        $this->facade    = $facade;
        $this->webLoader = $webLoader;
    }

    public function getTitle(): SimpleTranslation|string
    {
        return 'newsletter-email.front.subscribe.title';
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        $newsletterEmail = $this->facade->create($values->email);

        if ($newsletterEmail !== null) {
            $this->onFlashmessage('form.newsletter-email.front.subscribe.flash.success.create', Flash::SUCCESS);
            $this->onSendEmail($newsletterEmail);
        } else {
            $this->onFlashmessage('form.newsletter-email.front.subscribe.flash.danger.create', Flash::DANGER);
        }

        $this->onSuccess($form, $values, $form->isSubmitted()->name);
        $form->reset();
        $this->onRedraw();
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile($this->getControlTemplate(__DIR__ . '/formSubscribe.latte'));

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addEmail('email', 'form.newsletter-email.front.subscribe.email')
            ->setHtmlAttribute('placeholder', 'form.newsletter-email.front.subscribe.email.placeholder')
            ->setRequired('form.newsletter-email.front.subscribe.email.req');

        // BUTTON
        $form->addSubmit('send', 'form.newsletter-email.front.subscribe.send');

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];
        $this->onModifyForm($form);

        return $form;
    }
}
