<?php

declare(strict_types=1);

namespace Skadmin\NewsletterEmail\Components\Front;

interface IFormSubscribeFactory
{
    public function create(): FormSubscribe;
}
