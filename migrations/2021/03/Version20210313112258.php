<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210313112258 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'newsletter-email.overview', 'hash' => '43ac96e96844ae4fe07b673c5e1c7c32', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Newsletter', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.newsletter-email.title', 'hash' => '42531a7e4457f5718ca2e1498dd2aa49', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Newsletter', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.newsletter-email.description', 'hash' => 'e6dbbfec2d8513a10332bcbfa5fcd271', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat newsletter', 'plural1' => '', 'plural2' => ''],
            ['original' => 'newsletter-email.overview.title', 'hash' => 'a7ef036128382406b7e8db887abffac4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Newsletter|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.newsletter-email.overview.is-active', 'hash' => '418342afd5c575f8437721c592cde56e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Potvrzený', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.newsletter-email.overview.csv-export.title', 'hash' => '9218f1664afa2c3cacfe65c1245a9d3e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vyexportovat záznamy do souboru csv', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.newsletter-email.overview.csv-export', 'hash' => '84eb2be02778624f0c8882afc162e535', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Exportovat', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.newsletter-email.overview.email', 'hash' => 'bf035b94a4778a0458e9ad8e785435b9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'E-mail', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.newsletter-email.overview.created-at', 'hash' => '3ad326588f8b34a682d41bcec90e1f20', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vytvořeno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.newsletter-email.overview.remove', 'hash' => 'bdf4f8a40731c2092cecdab1ba835662', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.newsletter-email.overview.remove.title', 'hash' => 'fd4b98de62fa506bfce81384a9d384f9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Trvale smazat záznam', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.newsletter-email.overview.remove.confirmation - %s', 'hash' => '29c39c6f4dbb038b462e7539cbd73ab9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Opravdu chcete trvale smazat "%s"?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.newsletter-email.overview.export.email', 'hash' => '23496807829f0d934e60fca239860137', 'module' => 'admin', 'language_id' => 1, 'singular' => 'email', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.newsletter-email.overview.export.hash', 'hash' => '85ef03ed428eea77655bb115c8334b1b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'hash', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.newsletter-email.overview.export.is-active', 'hash' => '32901a7d9ef8079a2d46d98e8e5ac5c8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'active', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.newsletter-email.overview.export.created-at', 'hash' => '5fec3b59e094efc9b80c6d40492585e1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'created-at', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
