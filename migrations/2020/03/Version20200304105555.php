<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Nette\Utils\DateTime;
use Skadmin\NewsletterEmail\Mail\CMailNewsletterEmail;

use function serialize;
use function sprintf;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200304105555 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $mailTemplate = [
            'content'            => '',
            'parameters'         => serialize(CMailNewsletterEmail::getModelForSerialize()),
            'type'               => CMailNewsletterEmail::TYPE,
            'class'              => CMailNewsletterEmail::class,
            'name'               => sprintf('mail.%s.name', CMailNewsletterEmail::TYPE),
            'subject'            => sprintf('mail.%s.subject', CMailNewsletterEmail::TYPE),
            'last_update_author' => 'Cron',
            'last_update_at'     => new DateTime(),
            'recipients'         => '',
            'preheader'          => '',
        ];

        $this->addSql(
            'INSERT INTO mail_template (content, parameters, type, class, name, subject, last_update_author, last_update_at) VALUES (:content, :parameters, :type, :class, :name, :subject, :last_update_author, :last_update_at)',
            $mailTemplate
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
